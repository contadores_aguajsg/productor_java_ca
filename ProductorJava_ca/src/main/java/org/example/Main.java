package org.example;

import org.example.modelos.Productor;
import org.example.utilidades.Funciones;
import org.example.utilidades.Generador;


/** Por defecto, este proyecto simula 100.000 contadores de agua que envían pulsos de datos a un kafka.
 * Se genera un nuevo pulso de datos por cada contador cada 5 segundos durante 8 iteraciones.
 * Usando la programación concurrente se ha optimizado el proceso de generación de objetos Contador y el de envío de
 * mensajes al kafka para lograr una reducción significativa del tiempo de ejecución.
 * En mi caso, la ejecución de este programa con la configuración por defecto tarda 51 segundos.
 * Aunque para que se realicen las inserciones en Cassandra tarda bastante más, para hacer pruebas, se recomienda
 * cambiar los valores de TOTAL_CONTADORES e ITERACIONES por números pequeños
 * @Author José Sánchez García
 */
public class Main {

    public final static int TOTAL_CONTADORES = 100000;
    public final static int SEGUNDOS = 15;
    //Con 8 iteraciones se aprecian los cambios de color en cada ciclo
    public final static int ITERACIONES = 8; //Con 2 se ven genial los registros en cqlsh de Cassandra, para pruebas

    public static void main(String[] args) {

        Generador generador = new Generador();
        Funciones funciones = new Funciones(generador);
        Productor productor1 = new Productor(funciones, "key1", 1);

        if (productor1.isServerDisponible()) {
            funciones.iniciar(productor1);
        }
    }
}
