package org.example.utilidades;

import org.example.Main;
import org.example.modelos.Contador;
import org.json.JSONObject;

import java.util.Vector;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Clase generadora de recursos necesarios para crear la simulación
 */
public class Generador {

    private final Vector<Contador> contadores;

    /**
     * Devuelve de forma sincronizada, una cadena que representa un número aleatorio en el rango introducido, tendrá 8 decimales
     */
    private synchronized String generaAleatorio(int min, int max) {

        return String.format("%.8f", (Math.random() * (max - min + 1) + min));
    }

    /**
     * Devuelve la zona calculada, la que le corresponda según su latitud y longitud
     */
    private int generaZona(double latitud, double longitud) {

        return (latitud > 0 && longitud > 0) ? 2 :
                (latitud < 0 && longitud < 0) ? 3 :
                 (latitud > 0 && longitud < 0) ? 1 : 4;
    }

    /**
     * Genera una nueva medición para el contador que le llegue como argumento
     */
    public void generaMedicion(Contador contador) {


        //Se saca un aleatorio de 1 a 4, si su valor es 4, se creará un outlier
        boolean crearOutlier = (Funciones.conversor(generaAleatorio(1, 4)) == 4);

        if (!crearOutlier) {
            //Mediciones normales: Se considera normal un valor entre 1 y 5
            contador.anyadirMedicion(Funciones.conversor(generaAleatorio(1, 5)));
        } else {
            //Mediciones anormales: Un valor superior se considera outlier
            contador.anyadirMedicion(Funciones.conversor(generaAleatorio(6, 999)));
        }
    }

    /**
     * Genera un nuevo pulso para cada uno de los contadores.
     */
    public void generaMediciones(Vector<Contador> contadores) {

        //Genero una nueva medición para cada contador
        contadores.forEach(this::generaMedicion);
    }

    /**
     * Genera y devuelve una LinkedList con la cantidad de contadores que se indique como parámetro.
     * Los contadores se generan de una forma semi-aleatoria
     */
    private void generaContadores() {

        AtomicInteger iteracionAtomica = new AtomicInteger(0);

        while (contadores.size() < Main.TOTAL_CONTADORES) {
            //Voy añadiendo los objetos Contador generados
            this.contadores.addElement(generaContador(iteracionAtomica.incrementAndGet()));
        }
        //Comunico que ha terminado el proceso de generar contadores
        synchronized (contadores) {
            this.contadores.notify();
        }
    }

    /**
     * Genera un contador de agua y lo devuelve
     */
    private Contador generaContador(int id) {

        double latitud = Double.parseDouble(generaAleatorio(-49, 49).replace(",", "."));
        double longitud = Double.parseDouble(generaAleatorio(-49, 49).replace(",", "."));

        return new Contador(id, generaZona(latitud, longitud), latitud, longitud,
                Contador.Modelo.values()[ Funciones.conversor(generaAleatorio(0, 2))]);
    }

    /**
     * Empaqueta los datos que recibe como parámetro en formato Json y lo devuelve
     */
    public JSONObject generaJson(String idRegistro, int idContador, Contador.Modelo modelo, int caudal, long fecha, double latitud, double longitud, int zona) {

        JSONObject json = new JSONObject();
        JSONObject jsonMediciones = new JSONObject();

        //De esta manera se evitan errores con la detección automática de tipos de los json
        json.put("idRegistro", String.valueOf(idRegistro));
        json.put("idContador", idContador);
        json.put("longitud", longitud);
        json.put("latitud", latitud);
        json.put("modelo", modelo);
        json.put("zona", zona);

        jsonMediciones.put("fecha", fecha);
        jsonMediciones.put("caudal", caudal);

        json.put("mediciones", jsonMediciones);

        return json;
    }

    /**
     * Devuelve un hilo que genera los pulsos de datos de cada contador
     */
    public Thread generarDatos(Vector<Contador> vector1, Vector<Contador> vector2, Vector<Contador> vector3) {

        return new Thread(() -> {
            generaMediciones(vector1);
            generaMediciones(vector2);
            generaMediciones(vector3);
        });
    }

    /**
     * Devuelve una cadena que representa un ANSI_COLOR para imprimir por pantalla cada ciclo de un color distinto
     */
    public String generaColor(int iteracion) {
        return "\u001B[3" + iteracion + "m";
    }

    public Vector<Contador> getContadores() {
        return contadores;
    }

    //Constructor
    public Generador() {
        this.contadores = new Vector<>();
        generaContadores();
    }
}
