package org.example.utilidades;

import org.example.Main;
import org.example.modelos.Contador;
import org.example.modelos.Hilo;
import org.example.modelos.Productor;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Contiene funciones útiles para la ejecución de la simulación de los pulsos de los contadores de agua
 */
public class Funciones {

    private Generador generador;
    private Productor[] productores;

    /**
     * Inicia el proceso de simulación, esta es la función principal de ejecución del programa
     */
    public void iniciar(Productor productor1) {

        //Instancio los productores que se van a utilizar
        productores[0] = productor1;
        productores[1] = new Productor(this, "key1", 2);
        productores[2] = new Productor(this, "key1", 3);

        //Inicialmente, se generan todos los contadores
        Vector<Contador> contadoresList = this.generador.getContadores();

        // Bloque syncronized optimizado creado para esperar hasta que se generen los objetos Contador solicitados
        synchronized (contadoresList) {
            while (contadoresList.size() < Main.TOTAL_CONTADORES) {
                try {
                    contadoresList.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        //Gastaré el robo de trabajo para ejecutar 3 producer en paralelo
        ExecutorService executorService = Executors.newWorkStealingPool();
        //Vectores que contendrán los contadores que se asignarán a cada uno de los producer
        Vector<Contador> vectorContadores1 = new Vector<>();
        Vector<Contador> vectorContadores2 = new Vector<>();
        Vector<Contador> vectorContadores3 = new Vector<>();

        //Rellena los vectores de forma equitativa para cada producer
        for (int j = 0; j < contadoresList.size(); j++) {

            repartirContadores(j, contadoresList, vectorContadores1, vectorContadores2, vectorContadores3);
        }

        TimeUnit timeUnitMilis = TimeUnit.MILLISECONDS;
        //Tiempo de espera para generar el siguiente pulso de datos
        long milis = timeUnitMilis.convert(Main.SEGUNDOS, TimeUnit.SECONDS);
        Thread hilo;

        //Bucle que genera las mediciones de cada contador
        for (int i = 0; i < Main.ITERACIONES; i++) {

            //Generar nuevos datos para todos los contadores en segundo plano
            hilo = generador.generarDatos(vectorContadores1, vectorContadores2, vectorContadores3);
            hilo.start();

            try {
                hilo.join(); //Espero a que termine la ejecución del hilo que genera datos
            } catch (InterruptedException e) {
                System.err.println(e.getMessage());
            }

            //Envío de datos al kafka
            executorService.execute(new Hilo("1", vectorContadores1, this, productores[0]));
            executorService.execute(new Hilo("2", vectorContadores2, this, productores[1]));
            executorService.execute(new Hilo("3", vectorContadores3, this, productores[2]));

            try {
                Thread.sleep(milis); //Simulo el tiempo de espera entre cada pulso de datos que nos llega
            } catch (InterruptedException e) {
                System.err.println(e.getMessage());
            }
            System.out.println(generador.generaColor(i) + "<<<<<<<<<<NUEVO CICLO DE GENERACIÓN Y ENVÍO DE DATOS>>>>>>>>>>");
        }
        //Al finalizar, solicito el cierre de los recursos
        cerrarProducers();
        executorService.shutdown();
    }

    /**
     * Cierra los producer
     */
    private void cerrarProducers() {

        for (Productor productor : productores) {
            productor.cerrar();
        }
    }

    /**
     * Envía al kafka server de forma sincronizada, un registro de cada uno de los contadores,
     * el cual contiene su última medición
     */
    public synchronized void enviarDatos(Contador contador, Productor productor) {

        //Considero outlier a todas las mediciones que tengan un valor mayor a 5
        String topic = contador.getUltimaMedicion() > 5 ? "incidencias" : "registros";
        JSONObject json = this.generador.generaJson(UUID.randomUUID().toString(), contador.getId(),
                contador.getModelo(), contador.getUltimaMedicion(), System.currentTimeMillis(),
                contador.getLatitud(), contador.getLongitud(), contador.getZona());
        productor.enviarJson(json, topic);
    }


    /**
     * Comprueba si está disponible el broker de kafka
     */
    public boolean compruebaConexion(String ipHost, int puertoKafka) {

        try (Socket socket = new Socket()) {
            // Establecer un tiempo de espera para la conexión
            int timeoutMs = 5000;
            socket.connect(new InetSocketAddress(ipHost, puertoKafka), timeoutMs);
            System.out.println("El broker de Kafka está disponible\nConexión establecida\n");
            return true;
        } catch (Exception e) {
            System.err.println("No se pudo conectar al broker de Kafka");
            System.exit(1);
        }
        return false;
    }

    /**
     * Lee el fichero que le llegue como parámetro, mete los valores en un diccionario y lo devuelve.
     * En este caso se utiliza para obtener la ip pública y el puerto abierto del host de kafka de forma segura.
     *
     * @param nombreArchivo el cual queramos leer
     * @return un Map<String, String> con el contenido obtenido del archivo
     * @author José Sánchez García</autor>
     */
    public Map<String, String> obtenerDatos(String nombreArchivo) throws IOException {

        Map<String, String> hashMap = new HashMap<>();
        File file = new File("./src/main/resources/" + nombreArchivo);

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {

            String linea = br.readLine();

            while (linea != null) {
                hashMap.put(linea.split(" ")[0], linea.split(" ")[1]);
                linea = br.readLine();
            }
        }
        return hashMap;
    }

    /**
     * Añade a cada uno de los vectores una cantidad equitativa del total de contadores
     */
    private void repartirContadores(int iteracion, Vector<Contador> contadoresList, Vector<Contador> vector1, Vector<Contador> vector2, Vector<Contador> vector3) {

        if (iteracion < (contadoresList.size() / 3)) {
            vector1.add(contadoresList.get(iteracion));
        } else if (iteracion < (contadoresList.size() / 3) * 2) {
            vector2.add(contadoresList.get(iteracion));
        } else {
            vector3.add(contadoresList.get(iteracion));
        }
    }

    /**Convierte de forma sincronizada, una cadena numérica con decimales a un entero y lo devuelve*/
    public static synchronized int conversor(String cadenaNumerica){

       return Integer.parseInt(cadenaNumerica.substring(0, cadenaNumerica.indexOf(",")));
    }

    //Constructor
    public Funciones(Generador generador) {
        this.generador = generador;
        this.productores = new Productor[3];
    }
}

