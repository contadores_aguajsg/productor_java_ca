package org.example.modelos;

import java.util.LinkedList;
import java.util.List;

/**Representa un contador de agua*/
public class Contador {

    public enum Modelo {FENIX, ATLANTIS, HIDROJET}

    private int id;
    private int zona;
    private Modelo modelo;
    private double latitud;
    private double longitud;
    private List<Integer> mediciones;

    /**Añade una medición al listado de mediciones de este contador*/
    public void anyadirMedicion(int medicion){
        mediciones.add(medicion);
    }

    public int getId() {
        return id;
    }

    public Modelo getModelo() {
        return modelo;
    }

    public int getZona() {
        return zona;
    }

    public double getLatitud() {
        return latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    /**Devuelve la última medición añadida*/
    public Integer getUltimaMedicion(){
        return mediciones.get(mediciones.size() - 1);
    }

    @Override
    public String toString() {
        return "Contador{" +
                "id=" + id +
                ", zona=" + zona +
                ", modelo=" + modelo +
                ", mediciones=" + mediciones +
                '}';
    }

    //Constructor
    public Contador(int id, int zona, double latitud, double longitud, Modelo modelo) {
        this.id = id;
        this.zona = zona;
        this.modelo = modelo;
        this.latitud = latitud;
        this.longitud = longitud;
        //Como se van a hacer muchas inserciones y eliminaciones, elijo una linkedList
        this.mediciones = new LinkedList<>();
    }
}
