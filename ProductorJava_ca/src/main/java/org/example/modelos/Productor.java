package org.example.modelos;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.example.utilidades.Funciones;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

/**
 * Representa a un productor del kafka server
 */
public class Productor {

    private final Funciones funciones;
    private final Properties props;
    private final KafkaProducer producer;
    private String key;
    private boolean serverDisponible;

    /**
     * Envía el json que le llegue como parámetro al topic especificado
     */
    public void enviarJson(JSONObject json, String topic) {

        ProducerRecord<String, String> record = new ProducerRecord<>(topic, this.key, json.toString());
        this.producer.send(record);
    }

    /**Cierra el productor de kafka*/
    public void cerrar(){
        this.producer.close();
    }

    //Constructor
    public Productor(Funciones funciones, String key, int idGrupo) {

        this.serverDisponible = false;
        this.props = new Properties();
        this.funciones = funciones;
        this.key = key;

        //Gestiono la obtención de datos con un Optional inicialmente vacío
        Optional<Map<String, String>> mapOptional = Optional.empty();

        try {
            mapOptional = Optional.ofNullable(this.funciones.obtenerDatos("data.txt"));
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

        //Lanzaré una excepción si el optional es nulo
        String ipPublica = mapOptional.orElseThrow().get("ip_publica");
        int puertoAbierto = Integer.parseInt(mapOptional.orElseThrow().get("puerto_abierto"));

        //Configuración
        props.put("bootstrap.servers", ipPublica + ":" + puertoAbierto);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("group.id", idGrupo); //Para mandar mensajes con varios producer simultáneamente

        //Instancio un producer de kafka
        this.producer = new KafkaProducer<>(props);

        //Finalmente, compruebo si hay conexión con el kafka server
        this.serverDisponible = this.funciones.compruebaConexion(ipPublica, puertoAbierto);
        try {
            Thread.sleep(1000); //Espero durante un segundo para que sea visible el mensaje inicial
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public boolean isServerDisponible() {
        return serverDisponible;
    }
}

