package org.example.modelos;

import org.example.utilidades.Funciones;

import java.util.Vector;

/**Hilo personalizado para mandar mensajes al kafka con diferentes productores de forma sincronizada*/
public class Hilo extends Thread{

    private Vector<Contador> contadores;
    private Funciones funciones;
    private Productor productor;

    @Override
    public void run() {

        //Enviar los datos al kafka
        contadores.forEach((contador) -> {
            funciones.enviarDatos(contador, this.productor);
            //System.out.println("HILO" + getName() + " ----> MENSAJE ENVIADO:\n" + contador);
        });
    }

    //Constructor
    public Hilo(String threadName, Vector<Contador> contadores, Funciones funciones, Productor productor) {
        super(threadName);
        this.contadores = contadores;
        this.funciones = funciones;
        this.productor = productor;
    }
}
